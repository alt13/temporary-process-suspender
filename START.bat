@ECHO OFF

:: Logo
ECHO "  ___  ___ _   __ __   _____ 
ECHO "  |  \/  || | / //  | |____ |
ECHO "  | .  . || |/ / `| |     / /
ECHO "  | |\/| ||    \  | |     \ \
ECHO "  | |  | || |\  \_| |_.___/ /
ECHO "  \_|  |_/\_| \_/\___/\____/ 
ECHO "       gitlab.com/mk013
ECHO "        
                           
ECHO ==================================================
ECHO    PROCESS SUSPENDER STARTED
ECHO ==================================================
ECHO    Target:	GTA5.exe
ECHO    Time:	10s
ECHO ==================================================

:: Suspend process
pssuspend.exe GTA5.exe
:: Beep
rundll32.exe cmdext.dll,MessageBeepStub

:: Timer
ECHO    Timer started:
title timer
set timer=10
set time=%timer%
:loop
set /a time=%time% -1
if %time%==0 goto end
echo    %time% second(s)
ping localhost -n 2 >nul
goto loop
:end

:: Resume process
pssuspend.exe -r GTA5.exe
:: Beep
rundll32.exe cmdext.dll,MessageBeepStub

ECHO ==================================================
ECHO    PROCESS(ES) RESUMED
ECHO ==================================================

:: Exit timeout
timeout 10